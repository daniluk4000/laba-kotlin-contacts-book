package com.polytech.contactsbook

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class ContactAdapter(private val context: Context, private val dataSource: ArrayList<Contact>):BaseAdapter(){
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.contact, parent, false)

        val userName = rowView.findViewById(R.id.Name) as TextView;
        userName.text = "${getItem(position).lastName} ${getItem(position).firstName} ${getItem(position).surName}"

        return rowView
    }

    override fun getItem(position: Int): Contact {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

}